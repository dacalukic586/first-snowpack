/*ispisivanje u inspect element*/

import {helloWorld} from './hello-world.js';
helloWorld();

//******************************************** */
/*prikaz konfeti */
import confetti from 'canvas-confetti';
confetti.create(document.getElementById('canvas'), {
resize: true,
useWorker: true,
})({ particleCount: 200, spread: 200 });